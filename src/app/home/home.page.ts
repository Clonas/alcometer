import { Component } from '@angular/core';
import { DecimalPipe, NumberSymbol } from '@angular/common'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  genders = [];
  times = [];
  bottles = [];
  weight : number;
  gender : string;
  time : number;
  bottle : number;
  promilles : number;

  constructor() {}

  ngOnInit() {
    //Loops some numbers for times and bottles.
    for (let i = 1; i <= 100; i++) {
      this.times.push(i);
      this.bottles.push(i);
    }
      this.genders.push("Male");
      this.genders.push("Female");
      this.gender = "Male";
  }
  calculate() {
    // formula for calculating blood alcohol level
    const litres = this.bottle * 0.33;
    const grams = (litres * 8) * 4.5;
    const burning = this.weight / 10;
    const grams_left = grams - (burning * this.time);
    const result_male = grams_left / (this.weight * 0.7);
    const result_female = grams_left / (this.weight * 0.6);

    //check is it either male or female
    if (this.gender === "Male") {
      this.promilles = result_male;
    } else {
      this.promilles = result_female;
    }
  }

}
